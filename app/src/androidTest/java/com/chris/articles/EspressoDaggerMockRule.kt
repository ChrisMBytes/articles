package com.chris.articles

import androidx.test.platform.app.InstrumentationRegistry
import com.chris.articles.di.datasources.network.NetworkModule
import com.chris.articles.di.view.AppComponent
import com.chris.articles.view.ArticlesApplication
import it.cosenonjaviste.daggermock.DaggerMock

fun espressoDaggerMockRule() = DaggerMock.rule<AppComponent>(NetworkModule()) {
    set { component -> component.inject(app) }
    customizeBuilder<AppComponent.Builder> { it.application(app) }
}

val app: ArticlesApplication
    get() = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as ArticlesApplication