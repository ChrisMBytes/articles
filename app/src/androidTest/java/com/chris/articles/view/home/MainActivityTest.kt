package com.chris.articles.view.home

import android.content.Intent
import androidx.test.espresso.intent.Intents
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.chris.articles.datasources.network.articles.ArticlesNetServiceApi
import com.chris.articles.datasources.network.articles.models.ArticlesNet
import com.chris.articles.datasources.network.articles.models.ItemsNet
import com.chris.articles.espressoDaggerMockRule
import com.chris.articles.extensions.fromJson
import com.chris.articles.view.detail.DetailActivity
import com.chris.articles.view.home.robots.articleItem
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`

@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    @get:Rule
    var rule = espressoDaggerMockRule()
    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java, false, false)
    @Mock
    private lateinit var mockArticlesNetServiceApi: ArticlesNetServiceApi

    private lateinit var articles: ArticlesNet

    @Before
    fun setup() {
        val instrumentation = InstrumentationRegistry.getInstrumentation()
        articles = instrumentation.fromJson("articles.json")
        `when`(mockArticlesNetServiceApi.getArticles()).thenReturn(Single.just(articles))
        val item = instrumentation.fromJson<ItemsNet>("item.json")
        `when`(mockArticlesNetServiceApi.getItem(35)).thenReturn(Single.just(item))
    }

    @Test
    fun onLaunchDisplayPosts() {
        val (_, title1, subtitle1, _, date1) = articles.items!![0]
        val (_, title2, subtitle2, _, date2) = articles.items!![1]

        activityRule.launchActivity(Intent())

        articleItem {
            matchTitle(0, title1!!)
            matchSubtitle(0, subtitle1!!)
            matchDate(0, date1!!)
            matchTitle(1, title2!!)
            matchSubtitle(1, subtitle2!!)
            matchDate(1, date2!!)
        }
    }

    @Test
    fun onItemClickLaunchDetailActivity() {
        activityRule.launchActivity(Intent())
        Intents.init()

        articleItem {
            openOnClick(1, DetailActivity::class.java.name)
        }
    }
}