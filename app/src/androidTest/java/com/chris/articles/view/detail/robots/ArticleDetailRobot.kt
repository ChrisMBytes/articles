package com.chris.articles.view.detail.robots

import com.chris.articles.R
import com.chris.articles.robots.BaseTestRobot

fun articleDetail(func: ArticleDetailRobot.() -> Unit) = ArticleDetailRobot()
    .apply { func() }

class ArticleDetailRobot : BaseTestRobot() {
    fun matchTitle(title: String) = matchText(textView(R.id.text_title), title)

    fun matchBody(body: String) = matchText(textView(R.id.text_body), body)

    fun matchSubtitle(subtitle: String) = matchText(textView(R.id.text_subtitle), subtitle)

    fun matchDate(date: String) = matchText(textView(R.id.text_date), date)
}