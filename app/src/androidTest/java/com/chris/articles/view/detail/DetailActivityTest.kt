package com.chris.articles.view.detail

import android.content.Intent
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.chris.articles.datasources.network.articles.ArticlesNetServiceApi
import com.chris.articles.datasources.network.articles.models.ItemsNet
import com.chris.articles.espressoDaggerMockRule
import com.chris.articles.extensions.fromJson
import com.chris.articles.view.detail.robots.articleDetail
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`

@RunWith(AndroidJUnit4::class)
class DetailActivityTest {
    @get:Rule
    var rule = espressoDaggerMockRule()
    @get:Rule
    val activityRule = ActivityTestRule(DetailActivity::class.java, false, false)
    @Mock
    private lateinit var mockArticlesNetServiceApi: ArticlesNetServiceApi

    private fun launch(id: Int) {
        val intent = Intent()
        intent.putExtra(DetailActivity.EXTRA_ARTICLE_ID, id)
        activityRule.launchActivity(intent)
    }

    @Test
    fun onLaunchDisplayArticleDetailData() {
        val instrumentation = InstrumentationRegistry.getInstrumentation()
        val id = 35
        val item = instrumentation.fromJson<ItemsNet>("item.json")
        `when`(mockArticlesNetServiceApi.getItem(id)).thenReturn(Single.just(item))
        val (_, title, subtitle, body, date) = item.item

        launch(id)

        articleDetail {
            matchTitle(title!!)
            matchSubtitle(subtitle!!)
            matchBody(body!!)
            matchDate(date!!)
        }
    }
}