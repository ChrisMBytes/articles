package com.chris.articles.view.home.robots

import com.chris.articles.R
import com.chris.articles.robots.BaseTestRobot

fun articleItem(func: ArticleItemRobot.() -> Unit) = ArticleItemRobot()
    .apply { func() }

class ArticleItemRobot : BaseTestRobot() {
    fun matchTitle(position: Int, title: String) = matchListItem(R.id.articles_recycler_view, position, title)

    fun matchSubtitle(position: Int, subtitle: String) = matchListItem(R.id.articles_recycler_view, position, subtitle)

    fun matchDate(position: Int, date: String) = matchListItem(R.id.articles_recycler_view, position, date)

    fun openOnClick(position: Int, activityName: String) {
        clickListItem(R.id.articles_recycler_view, position)
        launchActivity(activityName)
    }
}