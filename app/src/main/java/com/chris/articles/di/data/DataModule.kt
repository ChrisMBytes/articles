package com.chris.articles.di.data

import com.chris.articles.data.articles.ArticlesRepository
import com.chris.articles.data.articles.mappers.ArticlesMapper
import com.chris.articles.data.articles.mappers.ItemMapper
import com.chris.articles.data.mappers.DomainMapperApi
import com.chris.articles.datasources.network.articles.ArticlesNetServiceApi
import com.chris.articles.datasources.network.articles.models.ArticlesNet
import com.chris.articles.datasources.network.articles.models.ItemNet
import com.chris.articles.domain.articles.ArticlesRepositoryApi
import com.chris.articles.domain.articles.models.Articles
import com.chris.articles.domain.articles.models.Item
import dagger.Module
import dagger.Provides

@Module
class DataModule {
    @Provides
    fun providesCacheCaseStudiesRepositoryApi(
        articlesNetServiceApi: ArticlesNetServiceApi,
        articleDomainMapperApi: DomainMapperApi<ArticlesNet, Articles>,
        itemDomainMapperApi: DomainMapperApi<ItemNet, Item>
    ): ArticlesRepositoryApi {
        return ArticlesRepository(articlesNetServiceApi, articleDomainMapperApi, itemDomainMapperApi)
    }

    @Provides
    fun providesArticlesMapper(
        domainMapperApi: DomainMapperApi<ItemNet, Item>
    ): DomainMapperApi<ArticlesNet, Articles> {
        return ArticlesMapper(domainMapperApi)
    }

    @Provides
    fun providesCaseStudyMapper(): DomainMapperApi<ItemNet, Item> {
        return ItemMapper()
    }
}