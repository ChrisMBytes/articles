package com.chris.articles.di.view

import android.app.Application
import com.chris.articles.di.data.DataModule
import com.chris.articles.di.datasources.network.NetworkModule
import com.chris.articles.di.domain.DomainModule
import com.chris.articles.di.presentation.PresentationModule
import com.chris.articles.di.presentation.ViewModelModule
import com.chris.articles.view.ArticlesApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBindingModule::class,
        ViewModelModule::class,
        PresentationModule::class,
        DomainModule::class,
        DataModule::class,
        NetworkModule::class
    ]
)
interface AppComponent : AndroidInjector<ArticlesApplication> {
    override fun inject(instance: ArticlesApplication)

    @Component.Builder
    interface Builder {
        fun networkModule(networkModule: NetworkModule): Builder

        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}