package com.chris.articles.di.datasources.network

import com.chris.articles.datasources.network.articles.ArticlesNetService
import com.chris.articles.datasources.network.articles.ArticlesNetServiceApi
import com.chris.articles.datasources.network.retrofit.RetrofitServiceGenerator
import com.chris.articles.datasources.network.retrofit.articles.ArticlesConfig
import com.chris.articles.datasources.network.retrofit.articles.ArticlesRestApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkModule {
    @Provides
    fun providesArticlesNetServiceApi(articlesRestApi: ArticlesRestApi): ArticlesNetServiceApi {
        return ArticlesNetService(articlesRestApi)
    }

    @Singleton
    @Provides
    fun providesArticlesRestApi(articlesConfig: ArticlesConfig): ArticlesRestApi {
        return RetrofitServiceGenerator(articlesConfig).createService(ArticlesRestApi::class.java)
    }

    @Provides
    fun providesArticlesConfig(): ArticlesConfig {
        return ArticlesConfig()
    }
}