package com.chris.articles.di.presentation

import com.chris.articles.domain.articles.interactors.GetArticleItemApi
import com.chris.articles.domain.articles.interactors.GetArticlesApi
import com.chris.articles.presentation.detail.viewmodels.DetailViewModel
import com.chris.articles.presentation.home.viewmodels.HomeViewModel
import com.chris.articles.rx.disposables.CompositeDisposableHandler
import com.chris.articles.rx.disposables.DisposableHandler
import dagger.Module
import dagger.Provides

@Module
class PresentationModule {
    @Provides
    fun providesHomeViewModel(
        getArticlesApi: GetArticlesApi,
        disposableHandler: DisposableHandler
    ): HomeViewModel {
        return HomeViewModel(getArticlesApi, disposableHandler)
    }

    @Provides
    fun providesDetailViewModel(
        getArticleItemApi: GetArticleItemApi,
        disposableHandler: DisposableHandler
    ): DetailViewModel {
        return DetailViewModel(getArticleItemApi, disposableHandler)
    }

    @Provides
    fun providesDisposableHandler(): DisposableHandler {
        return CompositeDisposableHandler()
    }
}