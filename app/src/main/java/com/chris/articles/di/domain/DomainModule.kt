package com.chris.articles.di.domain

import com.chris.articles.domain.articles.ArticlesRepositoryApi
import com.chris.articles.domain.articles.interactors.GetArticleItem
import com.chris.articles.domain.articles.interactors.GetArticleItemApi
import com.chris.articles.domain.articles.interactors.GetArticles
import com.chris.articles.domain.articles.interactors.GetArticlesApi
import dagger.Module
import dagger.Provides

@Module
class DomainModule {

    @Provides
    fun providesGetArticlesApi(articlesRepositoryApi: ArticlesRepositoryApi): GetArticlesApi {
        return GetArticles(articlesRepositoryApi)
    }

    @Provides
    fun providesGetArticleItemApi(articlesRepositoryApi: ArticlesRepositoryApi): GetArticleItemApi {
        return GetArticleItem(articlesRepositoryApi)
    }
}