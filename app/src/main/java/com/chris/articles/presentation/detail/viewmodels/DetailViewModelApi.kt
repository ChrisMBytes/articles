package com.chris.articles.presentation.detail.viewmodels

import androidx.lifecycle.MutableLiveData
import com.chris.articles.presentation.viewmodels.StateViewModel

interface DetailViewModelApi : StateViewModel {
    val date: MutableLiveData<String>
    val subtitle: MutableLiveData<String>
    val body: MutableLiveData<String>
    val title: MutableLiveData<String>
    val id: MutableLiveData<Int>
    val error: MutableLiveData<String>

    fun loadData(id: Int)
}