package com.chris.articles.presentation.recyclerview.articles.viewmodels

import androidx.lifecycle.MutableLiveData
import com.chris.articles.domain.articles.models.Item

class ArticleItemViewModel : ArticleItemViewModelApi {
    override val date = MutableLiveData<String>()
    override val subtitle = MutableLiveData<String>()
    override val title = MutableLiveData<String>()
    override val id = MutableLiveData<Int>()

    override fun setItem(item: Item) {
        date.value = item.date
        subtitle.value = item.subtitle
        title.value = item.title
        id.value = item.id
    }
}