package com.chris.articles.presentation.detail.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chris.articles.domain.articles.interactors.GetArticleItemApi
import com.chris.articles.domain.articles.models.Item
import com.chris.articles.rx.applyIoScheduler
import com.chris.articles.rx.disposables.DisposableHandler

class DetailViewModel(
    private val getArticleItemApi: GetArticleItemApi,
    private val disposableHandler: DisposableHandler
) : ViewModel(), DetailViewModelApi, DisposableHandler by disposableHandler {
    override val date = MutableLiveData<String>()
    override val body = MutableLiveData<String>()
    override val subtitle = MutableLiveData<String>()
    override val title = MutableLiveData<String>()
    override val id = MutableLiveData<Int>()
    override val error = MutableLiveData<String>()

    override fun loadData(id: Int) {
        addDisposable(
            getArticleItemApi.getItem(id)
                .applyIoScheduler()
                .subscribe(
                    { setData(it) },
                    { error.postValue(it.message) }
                )
        )
    }

    private fun setData(item: Item) {
        date.value = item.date
        body.value = item.body
        subtitle.value = item.subtitle
        title.value = item.title
        id.value = item.id
    }

    override fun clear() {
        clearDisposables()
    }
}