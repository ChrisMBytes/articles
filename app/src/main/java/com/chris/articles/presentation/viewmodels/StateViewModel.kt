package com.chris.articles.presentation.viewmodels

interface StateViewModel {
    fun clear()
}