package com.chris.articles.presentation.recyclerview.articles.viewmodels

import androidx.lifecycle.MutableLiveData
import com.chris.articles.domain.articles.models.Item
import com.chris.articles.presentation.recyclerview.viewmodels.ItemViewModel

interface ArticleItemViewModelApi : ItemViewModel<Item> {
    val date: MutableLiveData<String>
    val subtitle: MutableLiveData<String>
    val title: MutableLiveData<String>
    val id: MutableLiveData<Int>
}