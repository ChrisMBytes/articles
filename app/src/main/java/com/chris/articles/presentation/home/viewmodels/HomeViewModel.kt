package com.chris.articles.presentation.home.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chris.articles.domain.articles.interactors.GetArticlesApi
import com.chris.articles.domain.articles.models.Item
import com.chris.articles.rx.applyIoScheduler
import com.chris.articles.rx.disposables.DisposableHandler

class HomeViewModel(
    private val getArticlesApi: GetArticlesApi,
    private val disposableHandler: DisposableHandler
) : ViewModel(), HomeViewModelApi, DisposableHandler by disposableHandler {
    override val items = MutableLiveData<List<Item>>()
    override val error = MutableLiveData<String>()

    init {
        loadData()
    }

    private fun loadData() {
        addDisposable(
            getArticlesApi.getArticles()
                .applyIoScheduler()
                .subscribe(
                    { items.postValue(it.items) },
                    { error.postValue(it.message) }
                )
        )
    }

    override fun onCleared() {
        clearDisposables()
        super.onCleared()
    }

    override fun clear() {
        clearDisposables()
    }
}