package com.chris.articles.presentation.home.viewmodels

import androidx.lifecycle.MutableLiveData
import com.chris.articles.domain.articles.models.Item
import com.chris.articles.presentation.viewmodels.StateViewModel

interface HomeViewModelApi : StateViewModel {
    val items: MutableLiveData<List<Item>>
    val error: MutableLiveData<String>
}