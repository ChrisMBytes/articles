package com.chris.articles.presentation.recyclerview.viewmodels

interface ItemViewModel<ITEM_T> {

    fun setItem(item: ITEM_T)
}
