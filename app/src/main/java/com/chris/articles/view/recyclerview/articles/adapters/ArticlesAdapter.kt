package com.chris.articles.view.recyclerview.articles.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.chris.articles.R
import com.chris.articles.databinding.ArticleItemBinding
import com.chris.articles.domain.articles.models.Item
import com.chris.articles.presentation.recyclerview.articles.viewmodels.ArticleItemViewModel
import com.chris.articles.presentation.recyclerview.articles.viewmodels.ArticleItemViewModelApi
import com.chris.articles.view.recyclerview.adapters.DataBindingRecyclerViewAdapter
import com.chris.articles.view.recyclerview.articles.viewholders.ArticleItemViewHolder
import com.chris.articles.view.recyclerview.viewholders.ItemViewHolder

class ArticlesAdapter : DataBindingRecyclerViewAdapter<Item, ArticleItemViewModelApi>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemViewHolder<Item, ArticleItemViewModelApi> {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.article_item, parent, false)
        val viewModel = ArticleItemViewModel()
        val binding = ArticleItemBinding.bind(itemView)
        binding.viewModel = viewModel

        return ArticleItemViewHolder(itemView, binding, viewModel)
    }
}