package com.chris.articles.view.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.chris.articles.R
import com.chris.articles.databinding.ActivityDetailBinding
import com.chris.articles.presentation.detail.viewmodels.DetailViewModel
import com.chris.articles.presentation.detail.viewmodels.DetailViewModelApi
import dagger.android.AndroidInjection
import javax.inject.Inject

class DetailActivity : AppCompatActivity() {
    companion object Intent {
        const val EXTRA_ARTICLE_ID = "article_id"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val detailViewModelApi: DetailViewModelApi by lazy {
        ViewModelProviders.of(this, viewModelFactory)[DetailViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityDetailBinding>(
            this,
            R.layout.activity_detail
        )
        binding.lifecycleOwner = this
        AndroidInjection.inject(this)
        binding.viewModel = detailViewModelApi
        setup()
    }

    private fun setup() {
        val extras = intent.extras ?: return
        with(extras) {
            detailViewModelApi.apply {
                loadData(getInt(EXTRA_ARTICLE_ID))
            }
        }
    }

    override fun onDestroy() {
        detailViewModelApi.clear()
        super.onDestroy()
    }
}