package com.chris.articles.view.recyclerview.adapters.diffutils

import androidx.recyclerview.widget.DiffUtil

class DataClassListItemCallback<ITEM> : DiffUtil.ItemCallback<ITEM>() {
    override fun areItemsTheSame(oldItem: ITEM, newItem: ITEM): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: ITEM, newItem: ITEM): Boolean {
        return oldItem == newItem
    }
}