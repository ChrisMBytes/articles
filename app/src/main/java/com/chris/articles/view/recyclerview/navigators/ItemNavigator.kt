package com.chris.articles.view.recyclerview.navigators

import android.content.Context
import android.content.Intent
import com.chris.articles.view.detail.DetailActivity
import com.chris.articles.view.detail.DetailActivity.Intent.EXTRA_ARTICLE_ID

class ItemNavigator {
    companion object {
        @JvmStatic
        fun goToDetail(context: Context, articleId: Int?) {
            Intent(context, DetailActivity::class.java).apply {
                putExtra(EXTRA_ARTICLE_ID, articleId)
                context.startActivity(this)
            }
        }
    }
}