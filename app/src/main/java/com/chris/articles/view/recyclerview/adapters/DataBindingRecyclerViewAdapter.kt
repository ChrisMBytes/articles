package com.chris.articles.view.recyclerview.adapters

import androidx.recyclerview.widget.ListAdapter
import com.chris.articles.presentation.recyclerview.viewmodels.ItemViewModel
import com.chris.articles.view.recyclerview.adapters.diffutils.DataClassListItemCallback
import com.chris.articles.view.recyclerview.viewholders.ItemViewHolder

abstract class DataBindingRecyclerViewAdapter<ITEM, VIEW_MODEL : ItemViewModel<ITEM>>
    : ListAdapter<ITEM, ItemViewHolder<ITEM, VIEW_MODEL>>(DataClassListItemCallback<ITEM>()) {

    override fun onBindViewHolder(holder: ItemViewHolder<ITEM, VIEW_MODEL>, position: Int) {
        holder.setItem(getItem(position))
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}
