package com.chris.articles.view.recyclerview.articles.viewholders

import android.view.View
import androidx.databinding.ViewDataBinding
import com.chris.articles.domain.articles.models.Item
import com.chris.articles.presentation.recyclerview.articles.viewmodels.ArticleItemViewModelApi
import com.chris.articles.view.recyclerview.viewholders.ItemViewHolder

class ArticleItemViewHolder(
    itemView: View,
    binding: ViewDataBinding,
    viewModel: ArticleItemViewModelApi
) : ItemViewHolder<Item, ArticleItemViewModelApi>(itemView, binding, viewModel)