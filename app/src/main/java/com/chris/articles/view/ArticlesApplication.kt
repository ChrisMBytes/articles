package com.chris.articles.view

import com.chris.articles.di.view.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class ArticlesApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }
}