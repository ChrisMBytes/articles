package com.chris.articles.view.home

import android.graphics.drawable.ClipDrawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.chris.articles.R
import com.chris.articles.databinding.ActivityMainBinding
import com.chris.articles.presentation.home.viewmodels.HomeViewModel
import com.chris.articles.presentation.home.viewmodels.HomeViewModelApi
import com.chris.articles.view.recyclerview.articles.adapters.ArticlesAdapter
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val homeViewModelApi: HomeViewModelApi by lazy {
        ViewModelProviders.of(this, viewModelFactory)[HomeViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(
            this,
            R.layout.activity_main
        )
        binding.lifecycleOwner = this
        AndroidInjection.inject(this)
        setup(binding)
    }

    private fun setup(binding: ActivityMainBinding) {
        val itemDecor = DividerItemDecoration(this, ClipDrawable.HORIZONTAL)
        binding.articlesRecyclerView.addItemDecoration(itemDecor)
        homeViewModelApi.items.observe({ this.lifecycle }) {
            if (binding.articlesRecyclerView.adapter == null) {
                binding.articlesRecyclerView.layoutManager = LinearLayoutManager(this)
                ArticlesAdapter().apply {
                    submitList(it)
                    binding.articlesRecyclerView.adapter = this
                }
            }
        }
    }
}
