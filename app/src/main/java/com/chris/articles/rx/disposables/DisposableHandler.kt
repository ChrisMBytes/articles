package com.chris.articles.rx.disposables

import io.reactivex.disposables.Disposable

interface DisposableHandler {
    fun addDisposable(disposable: Disposable)

    fun addDisposables(disposables: List<Disposable>)

    fun clearDisposables()
}