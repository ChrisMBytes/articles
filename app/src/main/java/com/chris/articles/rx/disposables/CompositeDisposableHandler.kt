package com.chris.articles.rx.disposables

import io.reactivex.disposables.Disposable

class CompositeDisposableHandler : DisposableHandler {

    private val disposableList = mutableListOf<Disposable>()

    override fun addDisposable(disposable: Disposable) {
        disposableList.add(disposable)
    }

    override fun addDisposables(disposables: List<Disposable>) {
        disposableList.addAll(disposables)
    }

    override fun clearDisposables() {
        disposableList.forEach { disposable ->
            if (!disposable.isDisposed) {
                disposable.dispose()
            }
        }
        disposableList.clear()
    }
}