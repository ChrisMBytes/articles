package com.chris.articles.domain.articles

import com.chris.articles.domain.articles.models.Articles
import com.chris.articles.domain.articles.models.Item
import io.reactivex.Single

interface ArticlesRepositoryApi {
    fun getArticles(): Single<Articles>

    fun getItem(id: Int): Single<Item>
}