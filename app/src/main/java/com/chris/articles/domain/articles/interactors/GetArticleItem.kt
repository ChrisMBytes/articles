package com.chris.articles.domain.articles.interactors

import com.chris.articles.domain.articles.ArticlesRepositoryApi
import com.chris.articles.domain.articles.models.Item
import io.reactivex.Single

class GetArticleItem(
    private val articlesRepositoryApi: ArticlesRepositoryApi
) : GetArticleItemApi {
    override fun getItem(id: Int): Single<Item> {
        return articlesRepositoryApi.getItem(id)
    }
}