package com.chris.articles.domain.articles.models

data class Item(
    val date: String,
    val id: Int,
    val subtitle: String,
    val body: String,
    val title: String
)