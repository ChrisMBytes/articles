package com.chris.articles.domain.articles.interactors

import com.chris.articles.domain.articles.ArticlesRepositoryApi
import com.chris.articles.domain.articles.models.Articles
import io.reactivex.Single

class GetArticles(
    private val articlesRepositoryApi: ArticlesRepositoryApi
) : GetArticlesApi {
    override fun getArticles(): Single<Articles> {
        return articlesRepositoryApi.getArticles()
    }
}