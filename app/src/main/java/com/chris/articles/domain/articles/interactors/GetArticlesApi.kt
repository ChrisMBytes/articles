package com.chris.articles.domain.articles.interactors

import com.chris.articles.domain.articles.models.Articles
import io.reactivex.Single

interface GetArticlesApi {
    fun getArticles(): Single<Articles>
}