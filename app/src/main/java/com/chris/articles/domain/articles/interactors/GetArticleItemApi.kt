package com.chris.articles.domain.articles.interactors

import com.chris.articles.domain.articles.models.Item
import io.reactivex.Single

interface GetArticleItemApi {
    fun getItem(id: Int): Single<Item>
}