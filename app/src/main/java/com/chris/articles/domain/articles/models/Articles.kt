package com.chris.articles.domain.articles.models

data class Articles(
    val items: List<Item>
)