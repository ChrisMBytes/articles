package com.chris.articles.datasources.network.retrofit.articles

import com.chris.articles.BuildConfig.SERVER_URL
import com.chris.articles.datasources.network.retrofit.RetrofitConfig

class ArticlesConfig : RetrofitConfig {
    override val baseUrl: String
        get() = SERVER_URL
}