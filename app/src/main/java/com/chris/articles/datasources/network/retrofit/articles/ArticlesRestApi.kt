package com.chris.articles.datasources.network.retrofit.articles

import com.chris.articles.datasources.network.articles.models.ArticlesNet
import com.chris.articles.datasources.network.articles.models.ItemsNet
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ArticlesRestApi {
    @GET("contentList.json")
    fun getArticles(): Single<ArticlesNet>

    @GET("content/{id}.json")
    fun getItem(@Path("id") id: Int): Single<ItemsNet>
}