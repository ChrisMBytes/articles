package com.chris.articles.datasources.network.retrofit

interface ServiceGenerator {
    fun <Service> createService(serviceClass: Class<Service>): Service
}
