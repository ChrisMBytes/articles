package com.chris.articles.datasources.network.articles

import com.chris.articles.datasources.network.articles.models.ArticlesNet
import com.chris.articles.datasources.network.articles.models.ItemsNet
import com.chris.articles.datasources.network.retrofit.articles.ArticlesRestApi
import io.reactivex.Single

class ArticlesNetService(
    private val articlesRestApi: ArticlesRestApi
) : ArticlesNetServiceApi {
    override fun getArticles(): Single<ArticlesNet> {
        return articlesRestApi.getArticles()
    }

    override fun getItem(id: Int): Single<ItemsNet> {
        return articlesRestApi.getItem(id)
    }
}