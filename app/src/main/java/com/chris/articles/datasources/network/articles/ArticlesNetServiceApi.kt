package com.chris.articles.datasources.network.articles

import com.chris.articles.datasources.network.articles.models.ArticlesNet
import com.chris.articles.datasources.network.articles.models.ItemsNet
import io.reactivex.Single
import retrofit2.http.Path

interface ArticlesNetServiceApi {
    fun getArticles(): Single<ArticlesNet>

    fun getItem(@Path("id") id: Int): Single<ItemsNet>
}