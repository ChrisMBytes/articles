package com.chris.articles.datasources.network.articles.models

data class ArticlesNet(
    val items: List<ItemNet>?
)