package com.chris.articles.datasources.network.articles.models

data class ItemNet(
    val id: Int?,
    val title: String?,
    val subtitle: String?,
    val body: String?,
    val date: String?
)