package com.chris.articles.datasources.network.retrofit

interface RetrofitConfig {
    val baseUrl: String
}
