package com.chris.articles.datasources.network.retrofit

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitServiceGenerator(private val retrofitConfig: RetrofitConfig) : ServiceGenerator {
    private lateinit var retrofit: Retrofit

    init {
        initialise()
    }

    private fun initialise() {
        retrofit = Retrofit.Builder()
            .baseUrl(retrofitConfig.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    override fun <Service> createService(serviceClass: Class<Service>): Service {
        return retrofit.create(serviceClass)
    }
}
