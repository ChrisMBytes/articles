package com.chris.articles.data.articles.mappers

import com.chris.articles.data.mappers.DomainMapperApi
import com.chris.articles.datasources.network.articles.models.ItemNet
import com.chris.articles.domain.articles.models.Item

class ItemMapper : DomainMapperApi<ItemNet, Item> {
    override fun toModel(dataModel: ItemNet): Item {
        return with(dataModel) {
            Item(
                date ?: "",
                id ?: 0,
                subtitle ?: "",
                body ?: "",
                title ?: ""
            )
        }
    }
}