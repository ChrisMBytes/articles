package com.chris.articles.data.articles.mappers

import com.chris.articles.data.mappers.DomainMapperApi
import com.chris.articles.datasources.network.articles.models.ArticlesNet
import com.chris.articles.datasources.network.articles.models.ItemNet
import com.chris.articles.domain.articles.models.Articles
import com.chris.articles.domain.articles.models.Item

class ArticlesMapper(
    private val domainMapperApi: DomainMapperApi<ItemNet, Item>
) : DomainMapperApi<ArticlesNet, Articles> {
    override fun toModel(dataModel: ArticlesNet): Articles {
        return with(dataModel) {
            Articles(
                items?.let { mapTo(it) } ?: emptyList()
            )
        }
    }

    private fun mapTo(item: List<ItemNet>): List<Item> {
        return item.map { domainMapperApi.toModel(it) }
    }
}