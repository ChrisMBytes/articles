package com.chris.articles.data.articles

import com.chris.articles.data.mappers.DomainMapperApi
import com.chris.articles.datasources.network.articles.ArticlesNetServiceApi
import com.chris.articles.datasources.network.articles.models.ArticlesNet
import com.chris.articles.datasources.network.articles.models.ItemNet
import com.chris.articles.domain.articles.ArticlesRepositoryApi
import com.chris.articles.domain.articles.models.Articles
import com.chris.articles.domain.articles.models.Item
import io.reactivex.Single

class ArticlesRepository(
    private val articlesNetServiceApi: ArticlesNetServiceApi,
    private val articleDomainMapperApi: DomainMapperApi<ArticlesNet, Articles>,
    private val itemDomainMapperApi: DomainMapperApi<ItemNet, Item>
) : ArticlesRepositoryApi {
    override fun getArticles(): Single<Articles> {
        return articlesNetServiceApi.getArticles()
            .map { articleDomainMapperApi.toModel(it) }
    }

    override fun getItem(id: Int): Single<Item> {
        return articlesNetServiceApi.getItem(id)
            .map { itemDomainMapperApi.toModel(it.item) }
    }
}