package com.chris.articles.data.mappers

interface DomainMapperApi<DataModel, DomainModel> {
    fun toModel(dataModel: DataModel): DomainModel
}