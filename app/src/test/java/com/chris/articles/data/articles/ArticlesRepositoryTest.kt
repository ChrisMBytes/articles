package com.chris.articles.data.articles

import com.chris.articles.data.mappers.DomainMapperApi
import com.chris.articles.datasources.network.articles.ArticlesNetServiceApi
import com.chris.articles.datasources.network.articles.models.ArticlesNet
import com.chris.articles.datasources.network.articles.models.ItemNet
import com.chris.articles.datasources.network.articles.models.ItemsNet
import com.chris.articles.domain.articles.models.Articles
import com.chris.articles.domain.articles.models.Item
import com.chris.articles.rules.RxImmediateSchedulerRule
import com.nhaarman.mockitokotlin2.never
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class ArticlesRepositoryTest {
//    @get:Rule
//    var rxImmediateSchedulerRule = RxImmediateSchedulerRule()
    @Mock
    private lateinit var mockArticlesNetServiceApi: ArticlesNetServiceApi
    @Mock
    private lateinit var mockArticleDomainMapperApi: DomainMapperApi<ArticlesNet, Articles>
    @Mock
    private lateinit var mockItemDomainMapperApi: DomainMapperApi<ItemNet, Item>
    @Mock
    private lateinit var mockArticlesNet: ArticlesNet
    @Mock
    private lateinit var mockArticles: Articles
    @Mock
    private lateinit var mockItemsNet: ItemsNet
    @Mock
    private lateinit var mockItemNet: ItemNet
    @Mock
    private lateinit var mockItem: Item
    @InjectMocks
    private lateinit var sut: ArticlesRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test // RxScheduler bug when sometimes running tests in parallel
    fun `getArticles should return Articles when network call is successful`() {
        `when`(mockArticleDomainMapperApi.toModel(mockArticlesNet)).thenReturn(mockArticles)
        `when`(mockArticlesNetServiceApi.getArticles()).thenReturn(Single.just(mockArticlesNet))

        sut.getArticles()
            .test()
            .assertComplete()
            .assertNoErrors()
            .assertValue {
                it == mockArticles
            }
        verify(mockArticlesNetServiceApi).getArticles()
        verify(mockArticleDomainMapperApi).toModel(mockArticlesNet)
    }

    @Test
    fun `getArticles should return error when network call fails`() {
        val error = Exception("error")
        `when`(mockArticlesNetServiceApi.getArticles()).thenReturn(Single.error(error))

        sut.getArticles()
            .test()
            .assertError(error)
        verify(mockArticlesNetServiceApi).getArticles()
        verify(mockArticleDomainMapperApi, never()).toModel(mockArticlesNet)
    }

    @Test // RxScheduler bug when sometimes running tests in parallel
    fun `getItem should return Item when network call is successful`() {
        val id = 1
        `when`(mockItemsNet.item).thenReturn(mockItemNet)
        `when`(mockItemDomainMapperApi.toModel(mockItemNet)).thenReturn(mockItem)
        `when`(mockArticlesNetServiceApi.getItem(id)).thenReturn(Single.just(mockItemsNet))

        sut.getItem(id)
            .test()
            .assertComplete()
            .assertNoErrors()
            .assertValue {
                it == mockItem
            }
        verify(mockArticlesNetServiceApi).getItem(id)
        verify(mockItemDomainMapperApi).toModel(mockItemNet)
    }

    @Test
    fun `getItem should return error when network call fails`() {
        val id = 1
        val error = Exception("error")
        `when`(mockArticlesNetServiceApi.getItem(id)).thenReturn(Single.error(error))

        sut.getItem(id)
            .test()
            .assertError(error)
        verify(mockArticlesNetServiceApi).getItem(id)
        verify(mockItemDomainMapperApi, never()).toModel(mockItemNet)
    }
}