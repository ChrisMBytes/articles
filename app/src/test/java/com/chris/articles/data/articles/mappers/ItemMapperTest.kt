package com.chris.articles.data.articles.mappers

import com.chris.articles.datasources.network.articles.models.ItemNet
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class ItemMapperTest {
    @Mock
    private lateinit var mockItemNet: ItemNet
    private val sut = ItemMapper()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `toModel should return domain model with expected values`() {
        val (title, subtitle, date, body) = listOf("title", "subtitle", "date", "body")
        val id = 1
        `when`(mockItemNet.title).thenReturn(title)
        `when`(mockItemNet.date).thenReturn(date)
        `when`(mockItemNet.subtitle).thenReturn(subtitle)
        `when`(mockItemNet.body).thenReturn(body)
        `when`(mockItemNet.id).thenReturn(id)

        val result = sut.toModel(mockItemNet)

        assertEquals(title, result.title)
        assertEquals(date, result.date)
        assertEquals(subtitle, result.subtitle)
        assertEquals(body, result.body)
        assertEquals(id, result.id)
    }

    @Test
    fun `toModel should return domain model with default values when data model has null values`() {
        val result = sut.toModel(mockItemNet)

        assertEquals("", result.title)
        assertEquals("", result.date)
        assertEquals("", result.subtitle)
        assertEquals("", result.body)
        assertEquals(0, result.id)
    }
}