package com.chris.articles.domain.articles.interactors

import com.chris.articles.domain.articles.ArticlesRepositoryApi
import com.chris.articles.domain.articles.models.Item
import com.chris.articles.rules.RxImmediateSchedulerRule
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class GetArticleItemTest {
    @get:Rule
    var rxImmediateSchedulerRule = RxImmediateSchedulerRule()
    @Mock
    private lateinit var mockArticlesRepositoryApi: ArticlesRepositoryApi
    @Mock
    private lateinit var mockItem: Item
    @InjectMocks
    private lateinit var sut: GetArticleItem

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `getItem should return Item when call is successful`() {
        val id = 1
        `when`(mockArticlesRepositoryApi.getItem(id)).thenReturn(Single.just(mockItem))

        sut.getItem(id)
            .test()
            .assertComplete()
            .assertNoErrors()
            .assertValue {
                it == mockItem
            }
        verify(mockArticlesRepositoryApi).getItem(id)
    }

    @Test
    fun `getItem should return error when call fails`() {
        val id = 1
        val error = Exception("error")
        `when`(mockArticlesRepositoryApi.getItem(id)).thenReturn(Single.error(error))

        sut.getItem(id)
            .test()
            .assertError(error)
        verify(mockArticlesRepositoryApi).getItem(id)
    }
}