package com.chris.articles.domain.articles.interactors

import com.chris.articles.domain.articles.ArticlesRepositoryApi
import com.chris.articles.domain.articles.models.Articles
import com.chris.articles.rules.RxImmediateSchedulerRule
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class GetArticlesTest {
    @get:Rule
    var rxImmediateSchedulerRule = RxImmediateSchedulerRule()
    @Mock
    private lateinit var mockArticlesRepositoryApi: ArticlesRepositoryApi
    @Mock
    private lateinit var mockArticles: Articles
    @InjectMocks
    private lateinit var sut: GetArticles

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `getArticles should return Articles when call is successful`() {
        `when`(mockArticlesRepositoryApi.getArticles()).thenReturn(Single.just(mockArticles))

        sut.getArticles()
            .test()
            .assertComplete()
            .assertNoErrors()
            .assertValue {
                it == mockArticles
            }
        verify(mockArticlesRepositoryApi).getArticles()
    }

    @Test
    fun `getArticles should return error when call fails`() {
        val error = Exception("error")
        `when`(mockArticlesRepositoryApi.getArticles()).thenReturn(Single.error(error))

        sut.getArticles()
            .test()
            .assertError(error)
        verify(mockArticlesRepositoryApi).getArticles()
    }
}