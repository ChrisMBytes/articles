package com.chris.articles.datasources.network.articles

import com.chris.articles.datasources.network.articles.models.ArticlesNet
import com.chris.articles.datasources.network.articles.models.ItemNet
import com.chris.articles.datasources.network.articles.models.ItemsNet
import com.chris.articles.datasources.network.retrofit.articles.ArticlesRestApi
import com.chris.articles.rules.RxImmediateSchedulerRule
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class ArticlesNetServiceTest {
    @get:Rule
    var rxImmediateSchedulerRule = RxImmediateSchedulerRule()
    @Mock
    private lateinit var mockArticlesRestApi: ArticlesRestApi
    @Mock
    private lateinit var mockArticlesNet: ArticlesNet
    @Mock
    private lateinit var mockItemsNet: ItemsNet
    @Mock
    private lateinit var mockItemNet: ItemNet
    @InjectMocks
    private lateinit var sut: ArticlesNetService

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `getArticles should return CaseStudies when network call is successful`() {
        val (title, subtitle, date) = listOf("title", "subtitle", "date")
        val items = listOf(mockItemNet)
        `when`(mockItemNet.title).thenReturn(title)
        `when`(mockItemNet.date).thenReturn(date)
        `when`(mockItemNet.subtitle).thenReturn(subtitle)
        `when`(mockArticlesNet.items).thenReturn(items)
        `when`(mockArticlesRestApi.getArticles()).thenReturn(Single.just(mockArticlesNet))

        sut.getArticles()
            .test()
            .assertComplete()
            .assertNoErrors()
            .assertValue {
                it.items == items
            }
        verify(mockArticlesRestApi).getArticles()
    }

    @Test
    fun `getArticles should return error when network call fails`() {
        val error = Exception("error")
        `when`(mockArticlesRestApi.getArticles()).thenReturn(Single.error(error))

        sut.getArticles()
            .test()
            .assertError(error)
        verify(mockArticlesRestApi).getArticles()
    }

    @Test
    fun `getItem should return CaseStudies when network call is successful`() {
        val (title, subtitle, body, date) = listOf("title", "subtitle", "body", "date")
        val id = 1
        `when`(mockItemNet.title).thenReturn(title)
        `when`(mockItemNet.date).thenReturn(date)
        `when`(mockItemNet.subtitle).thenReturn(subtitle)
        `when`(mockItemNet.body).thenReturn(body)
        `when`(mockItemNet.id).thenReturn(id)
        `when`(mockItemsNet.item).thenReturn(mockItemNet)
        `when`(mockArticlesRestApi.getItem(id)).thenReturn(Single.just(mockItemsNet))

        sut.getItem(id)
            .test()
            .assertComplete()
            .assertNoErrors()
            .assertValue {
                it.item == mockItemNet
            }
        verify(mockArticlesRestApi).getItem(id)
    }

    @Test
    fun `getItem should return error when network call fails`() {
        val id = 1
        val error = Exception("error")
        `when`(mockArticlesRestApi.getItem(id)).thenReturn(Single.error(error))

        sut.getItem(id)
            .test()
            .assertError(error)
        verify(mockArticlesRestApi).getItem(id)
    }
}