package com.chris.articles.presentation.home.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.chris.articles.domain.articles.interactors.GetArticlesApi
import com.chris.articles.domain.articles.models.Articles
import com.chris.articles.domain.articles.models.Item
import com.chris.articles.rules.RxImmediateSchedulerRule
import com.chris.articles.rx.disposables.DisposableHandler
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class HomeViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    @get:Rule
    var rxImmediateSchedulerRule = RxImmediateSchedulerRule()
    @Mock
    private lateinit var mockGetArticlesApi: GetArticlesApi
    @Mock
    private lateinit var mockDisposableHandler: DisposableHandler
    @Mock
    private lateinit var mockArticles: Articles
    @Mock
    private lateinit var mockItem: Item

    private lateinit var sut: HomeViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        `when`(mockArticles.items).thenReturn(listOf(mockItem))
        `when`(mockGetArticlesApi.getArticles()).thenReturn(Single.just(mockArticles))
    }

    @Test
    fun `init should set data when data fetched successfully`() {
        sut = HomeViewModel(mockGetArticlesApi, mockDisposableHandler)

        assertEquals(mockArticles.items, sut.items.value)
        verify(mockGetArticlesApi).getArticles()
        verify(mockDisposableHandler).addDisposable(any())
    }

    @Test
    fun `init should set error when call has error`() {
        val error = Exception("error")
        `when`(mockGetArticlesApi.getArticles()).thenReturn(Single.error(error))

        sut = HomeViewModel(mockGetArticlesApi, mockDisposableHandler)

        assertEquals(error.message, sut.error.value)
        verify(mockGetArticlesApi).getArticles()
        verify(mockDisposableHandler).addDisposable(any())
    }

    @Test
    fun `clear should make call to clear disposables`() {
        sut = HomeViewModel(mockGetArticlesApi, mockDisposableHandler)

        sut.clear()

        verify(mockDisposableHandler).clearDisposables()
    }
}