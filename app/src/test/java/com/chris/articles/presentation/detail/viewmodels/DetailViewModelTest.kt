package com.chris.articles.presentation.detail.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.chris.articles.domain.articles.interactors.GetArticleItemApi
import com.chris.articles.domain.articles.models.Item
import com.chris.articles.rules.RxImmediateSchedulerRule
import com.chris.articles.rx.disposables.DisposableHandler
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class DetailViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    @get:Rule
    var rxImmediateSchedulerRule = RxImmediateSchedulerRule()
    @Mock
    private lateinit var mockGetArticleItemApi: GetArticleItemApi
    @Mock
    private lateinit var mockDisposableHandler: DisposableHandler
    @Mock
    private lateinit var mockItem: Item
    @InjectMocks
    private lateinit var sut: DetailViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `loadData should set data to expected values when call is successful`() {
        val (title, subtitle, date, body) = listOf("title", "subtitle", "date", "body")
        val id = 1
        `when`(mockItem.title).thenReturn(title)
        `when`(mockItem.date).thenReturn(date)
        `when`(mockItem.subtitle).thenReturn(subtitle)
        `when`(mockItem.body).thenReturn(body)
        `when`(mockItem.id).thenReturn(id)
        `when`(mockGetArticleItemApi.getItem(id)).thenReturn(Single.just(mockItem))

        sut.loadData(id)

        assertEquals(title, sut.title.value)
        assertEquals(date, sut.date.value)
        assertEquals(subtitle, sut.subtitle.value)
        assertEquals(body, sut.body.value)
        assertEquals(id, sut.id.value)
        verify(mockGetArticleItemApi).getItem(id)
    }

    @Test
    fun `loadData should emmit error when call is unsuccessful`() {
        val id = 1
        val error = Exception("error")
        `when`(mockGetArticleItemApi.getItem(id)).thenReturn(Single.error(error))

        sut.loadData(id)

        assertEquals(error.message, sut.error.value)
        verify(mockGetArticleItemApi).getItem(id)
    }

    @Test
    fun `clear should make call to clear disposables`() {
        sut = DetailViewModel(mockGetArticleItemApi, mockDisposableHandler)

        sut.clear()

        verify(mockDisposableHandler).clearDisposables()
    }
}