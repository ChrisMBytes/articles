package com.chris.articles.presentation.recyclerview.articles.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.chris.articles.domain.articles.models.Item
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class ArticleItemViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    @Mock
    private lateinit var mockItem: Item
    private val sut = ArticleItemViewModel()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `setItem should set data to expected values`() {
        val (title, subtitle, date) = listOf("title", "subtitle", "date")
        val id = 1
        `when`(mockItem.title).thenReturn(title)
        `when`(mockItem.date).thenReturn(date)
        `when`(mockItem.subtitle).thenReturn(subtitle)
        `when`(mockItem.id).thenReturn(id)

        sut.setItem(mockItem)

        assertEquals(title, sut.title.value)
        assertEquals(date, sut.date.value)
        assertEquals(subtitle, sut.subtitle.value)
        assertEquals(id, sut.id.value)
    }
}